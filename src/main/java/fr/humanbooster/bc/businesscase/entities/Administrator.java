package fr.humanbooster.bc.businesscase.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Administrator extends User{
	
	@OneToMany(mappedBy = "administrator")
	private List<TechnicalTest> technicalTest;

	public Administrator() {
		// TODO Auto-generated constructor stub
	}

	public List<TechnicalTest> getTechnicalTest() {
		return technicalTest;
	}

	public void setTechnicalTest(List<TechnicalTest> technicalTest) {
		this.technicalTest = technicalTest;
	}

	@Override
	public String toString() {
		return "Administrator toString";
	}

	
	
}
