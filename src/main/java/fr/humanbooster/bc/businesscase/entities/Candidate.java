package fr.humanbooster.bc.businesscase.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Candidate extends User {

	private Long poleEmploiNum;
	
	@Temporal(TemporalType.DATE)
	private Date birthDate;
	
	@ManyToOne
	private InformationDay informationDay;
	
	public Candidate() {
		// TODO Auto-generated constructor stub
	}

	public Long getPoleEmploiNum() {
		return poleEmploiNum;
	}

	public void setPoleEmploiNum(Long poleEmploiNum) {
		this.poleEmploiNum = poleEmploiNum;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public InformationDay getInformationDay() {
		return informationDay;
	}

	public void setInformationDay(InformationDay informationDay) {
		this.informationDay = informationDay;
	}

	@Override
	public String toString() {
		return "Candidate [poleEmploiNum=" + poleEmploiNum + ", birthDate=" + birthDate + ", informationDay="
				+ informationDay + "]";
	}

	
	
}
