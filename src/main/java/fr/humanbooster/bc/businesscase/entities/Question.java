package fr.humanbooster.bc.businesscase.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Question {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String statement;
	private boolean isQualidier;
	
	@OneToMany(mappedBy = "question")
	private List<Response> responses;
	@ManyToOne
	private Theme theme;
	@ManyToOne
	private Level level;
	@ManyToOne
	private Media media;
	
	public Question() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public boolean isQualidier() {
		return isQualidier;
	}

	public void setQualidier(boolean isQualidier) {
		this.isQualidier = isQualidier;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", statement=" + statement + ", isQualidier="
				+ isQualidier + "]";
	}
	
}
