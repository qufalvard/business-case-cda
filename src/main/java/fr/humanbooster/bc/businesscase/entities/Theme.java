package fr.humanbooster.bc.businesscase.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Theme {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String entitled;
	
	@OneToMany(mappedBy = "theme")
	private List<Question> questions;
	@ManyToOne
	private TechnicalTest technicalTest;
	
	public Theme() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntitled() {
		return entitled;
	}

	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}

	@Override
	public String toString() {
		return "Theme [id=" + id + ", entitled=" + entitled + "]";
	}
	
}
